FROM node:slim AS build
WORKDIR /app
COPY app.js Board.vue index.html package.json vite.config.js ./
RUN yarn && yarn build

FROM golang:alpine
WORKDIR /app
COPY --from=build /app/dist/ /app/dist
COPY syncserver.go go.mod ./
RUN go mod download
ENV GIN_MODE=release
CMD ["go", "run", "./"]
