package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

type Points struct {
	Player1 []int `json:"0"`
	Player2 []int `json:"1"`
	Player3 []int `json:"2"`
	Player4 []int `json:"3"`
	Player5 []int `json:"4"`
	Player6 []int `json:"5"`
}

type Said struct {
	Player1 []int `json:"0"`
	Player2 []int `json:"1"`
	Player3 []int `json:"2"`
	Player4 []int `json:"3"`
	Player5 []int `json:"4"`
	Player6 []int `json:"5"`
}

type Values struct {
	Points Points `json:"values"`
	Said Said `json:"said"`
}

var store = map[string]Values{}

func main() {
	r := gin.Default()
	r.StaticFile("/", "./dist/index.html")
	r.StaticFile("/index.html", "./dist/index.html")
	r.Static("/assets", "./dist/assets/")
	r.GET("/sync/:code", get)
	r.PUT("/sync/:code", set)
	fmt.Printf("Starting on Port 8080\n")
	r.Run(":8080")
}

func get(c *gin.Context) {
	code := c.Param("code")
	val, exists := store[code];
	if !exists {
		fmt.Printf("Code %s is not in store\n", code)
		c.String(404, "There is no store with this code")
		return
	}
	fmt.Printf("Retrived values for code %s\n", code)
	c.JSON(200, val)
}

func set(c *gin.Context) {
	code := c.Param("code")
	val := Values{}
	err := c.BindJSON(&val)
	if err != nil {
		fmt.Println(err)
	}
	store[code] = val
	fmt.Printf("Stored values for %s\n", code)
	c.Status(200)
}